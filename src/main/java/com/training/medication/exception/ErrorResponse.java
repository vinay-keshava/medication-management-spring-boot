package com.training.medication.exception;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


@AllArgsConstructor
@Getter
@Setter
public class ErrorResponse {
	Long errorCode;
	List<String> errorMessage;
	

	
}

