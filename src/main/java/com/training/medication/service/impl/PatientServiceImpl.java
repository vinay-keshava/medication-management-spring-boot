package com.training.medication.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.medication.dto.PatientDto;
import com.training.medication.entity.Patient;
import com.training.medication.exception.CustomException;
import com.training.medication.repository.PatientRepository;
import com.training.medication.service.PatientService;

@Service
public class PatientServiceImpl implements PatientService {

	@Autowired
	PatientRepository patientRepository;

	@Override
	public List<Patient> getAllPatients() {
		return patientRepository.findAll();
	}

	@Override
	public Patient createNewPatient(PatientDto patientDto) {
		Patient patient=new Patient();
		patient.setAadharNumber(patientDto.getAadharNumber());
		patient.setAge(patientDto.getAge());
		patient.setEmail(patientDto.getEmail());
		patient.setFirstName(patientDto.getFirstName());
		patient.setLastName(patientDto.getLastName());
		patient.setPhoneNumber(patientDto.getPhoneNumber());
		return patientRepository.save(patient);
	}

	@Override
	public Patient findPatientById(long id) {
		Optional<Patient> patient = patientRepository.findById(id);
		if (patient.isPresent()) {
			return patient.get();
		}
		throw new CustomException("Patient with entered id not present");
	}

}
