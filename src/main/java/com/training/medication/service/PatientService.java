package com.training.medication.service;

import java.util.List;

import com.training.medication.dto.PatientDto;
import com.training.medication.entity.Patient;

public interface PatientService {

	List<Patient> getAllPatients();
	Patient findPatientById(long id);
	Patient createNewPatient(PatientDto patient);

}
