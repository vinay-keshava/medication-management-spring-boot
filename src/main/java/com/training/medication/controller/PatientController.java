package com.training.medication.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.medication.dto.PatientDto;
import com.training.medication.entity.Patient;
import com.training.medication.service.impl.PatientServiceImpl;

@RestController
@RequestMapping("/patients")
public class PatientController {

	@Autowired
	PatientServiceImpl patientSerImpl;
	
	@GetMapping("/")
	public ResponseEntity<List<Patient>> getAllPatients(){
		return new ResponseEntity<>(patientSerImpl.getAllPatients(),HttpStatus.OK);
	}
	
	@PostMapping("/")
	public ResponseEntity<Patient> addPatient(@Valid @RequestBody PatientDto patient){
		return new ResponseEntity<>(patientSerImpl.createNewPatient(patient),HttpStatus.OK);
	}
	
	@GetMapping("/{patientId}")
	public ResponseEntity<Patient> retreivePatientById(@PathVariable long patientId){
		return new ResponseEntity<>(patientSerImpl.findPatientById(patientId),HttpStatus.OK);
	}
}
