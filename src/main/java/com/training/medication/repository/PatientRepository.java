package com.training.medication.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.medication.entity.Patient;


public interface PatientRepository extends  JpaRepository<Patient, Long>{

}
