package com.training.medication.dto;

import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PatientDto {

	@NotBlank(message = "First Name is a required field")
	private String firstName;
	
	private String lastName;
	private String aadharNumber;
	
	@NotNull
	@Pattern(regexp = "[6-9]\\d{9}", message = "Please enter Valid Indian Phone Number")
	private String phoneNumber;
	
	@Email(message = "Please Enter Valid email", regexp = "^[a-zA-Z0-9_.-]+@[a-zA-Z0-9.-]+$")
	@NotNull
	@Column(unique = true)
	private String email;
		
	@NotNull (message = "Please input valid age")
	private int age;
}
